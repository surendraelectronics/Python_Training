Notion Link - https://www.notion.so/learn2020/Python-Training-70b7d88bcabc4638a917013e30fd842d

# ✍️ Prerequisite
- Github Account
- Notion

# 🐍 Python_Training
This Repository contains the details of the python concepts, day wise learnings and notebooks which helps to learn python.

# ✍️ What is Python?

- Python is a programming language and simple to use. It is used to develop GUI, Web App development(Flask,Django,Streamlit,Bottle), API Development(SOAP,REST,GraphQL..), Testing applications(Selenium,Robotframework,Cypress.io(Angular, node, react,endtoend js,RPA,Katlon,pytest,BDD,Cucumber,unittest), Machine Learning, Data Analytics (pandas,ploty,dash),Kiwi(mobile app),Work Automation,Appium...etc
- Python is a interpreter means codes are interpreted line by line at a time using the interpreter.
- Python is a dynamically typed means no need to define the type int,string,char n =10;fl=10.234
- Python has integration to many packages which makes so powerful.
- Python is cross-platform & free and open source programming language. 

# ✍️ How to start (Installation)

### 👉 Local - Single version
- Download the python3
- Select the user defined directory and select the path

### 👉 Local - Multiple version (Not Recommended)
- Install python 3.6 in user defined folder C:\Python36.
- Install python 3.7 in user defined folder C:\Python37.
- PYTHON36_HOME: C:\Python36, PYTHON37_HOME: C:\Python37
- Path: %PYTHON36_HOME%;%PYTHON36_HOME%\Scripts;%PYTHON37_HOME%;%PYTHON37_HOME%\Scripts;
- C:\Python36\python.exe → C:\Python36\python36.exe,C:\Python37\python.exe → C:\Python37\python37.exe
- python36 -m pip install package,python37 -m pip install package

### 👉 Notebooks (Anaconda)
- Jupyter  Notebook (ML & Data Analytics) — Run python html
- Jupyter Lab (ML & Data Analytics)
- Google Colab
- Deep Note
- Repl(Read-Evaluate-Print-loop)it [https://replit.com/~](https://replit.com/~)

### 👉 Mobile App
- Programming Hero

### 👉 IDE
- Visual Studio Code Editor(best)
- Py Charm
- Sublime 
- Atom
- Spider

### 👉 Virtual Environment
- python -m venv venv(name of the environment)

### Learning Plan (Tentative)
| Topics                                     | No of Days |
|--------------------------------------------|------------|
| Installation,Variables & Types             | 1          |
| Basic String Operations & String Formatting| 1          |
| Lists, Dictionaries & Sets                 | 2          |
| Basic Operators, Conditions, Loops         | 1          |
| Functions, List Comprehensions             | 1          |
| Classes & Objects                          | 2          |

